<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the main nav widget
    |
    */

    'login' => 'Login',
    'try_it_free' => 'Try if Free',
    'solutions' => 'Solutions',
    'functions' => 'Functions',
    'demo' => 'Demo',
    'document' => 'Document',
    'faq' => 'FAQ',
    'inquiry' => 'Inquiry',

];
