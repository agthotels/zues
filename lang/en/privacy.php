<?php

return [
    'title' => 'Privacy Policy',
    'desc' => 'Acomo Inc. (“company” “we” “our” and “us”) recognizes that protecting personal information is a social responsibility as a company, and in compliance with the related laws and regulations regarding the protection of personal information, company handles properly as follows.',
    'objectives' => [
        'title' => 'Objectives',
        'desc' => 'The personal information we collected will be used for the following purposes.',
        'l1' => 'To provide products, services and related information',
        'l2' => 'To improve products and services and develop new products and services',
        'l3' => 'To verify and confirm the use of products and services',
        'l4' => 'To properly and smoothly carry out transactions, contracts, and management',
        'l5' => 'To answer and respond to consultations and inquiries',
        'l6' => 'To apply for reservations or make necessary contact with hotels, facilities, and other related companies that use our products and services',
        'l7' => 'To inform about products and services of company and affiliates such as email and direct mail',
        'l8' => 'To conduct surveys, marketing data surveys, statistics and analysis',
        'l9' => 'To exercise rights and fulfill obligations under contracts and laws',
        'l10' => 'To perform various procedures related to personnel and labor management',
        'l11' => 'To carry out shareholder-related operations, such as holding and operating shareholder meetings and shipping shareholder benefits',
        'l12' => 'To carry out the operations related to the above items',
        'l13' => 'In addition, for the purpose of use with the consent of the person',
    ],
    'acquisition' => [
        'title' => 'Acquisition of personal information',
        'desc' => 'When we collect your personal information, we will acquire it by appropriate and legal means to the extent necessary for our business.',
    ],
    'provision' => [
        'title' => 'Voluntary provision of personal information',
        'desc' => 'It is up to you to decide whether or not to provide personal information to us. However, we may not be able to provide our services if we are unable to receive the necessary personal information for each of our services, or if the personal information provided is incomplete.',
    ],
    'management' => [
        'title' => 'Management of personal information',
        'desc' => 'Regarding the personal information you provide, we will take appropriate protection and safety measures in accordance with the relevant laws and regulations concerning the protection of personal information, and will endeavor to prevent the loss, destruction, falsification and leakage of personal information.',
    ],
    'personal-info' => [
        'title' => 'Use of personal information',
        'desc' => 'We will not use the personal information provided by you for any purpose other than the purpose for which we have notified you, unless otherwise stipulated by law.',
    ],
    'provision-personal-info' => [
        'title' => 'Provision of personal information to a third party',
        'desc' => 'We will not disclose or provide personal information to a third party except in the following cases.',
        'l1' => 'With the consent of the person',
        'l2' => 'When required by law',
        'l3' => 'When it is necessary for the protection of human life, health or property, and it is difficult to obtain the consent of the person',
        'l4' => 'When we need to cooperate with a national institution or local government or a person entrusted with it to carry out the affairs stipulated by law.',
        'l5' => 'When disclosing based on a warrant issued by the court or other judgment, decision or order of the court',
        'l6' => 'When information is inquired by lawful procedures from the prosecution, police, regulatory agencies, tax offices, consumer life centers, lawyers’ associations, or any other authority.',
        'l7' => 'In addition, when individually specified by our service',
    ],
    'sensitive-info' => [
        'title' => 'Sensitive personal information',
        'desc' => 'Regarding sensitive personal information (definition of Article 2, Paragraph 3 of the Act on the Protection of Personal Information), we will not obtain the relevant information unless there is prior consent of the person or based on the law.',
    ],
    'consignment-info' => [
        'title' => 'Consignment of personal information',
        'desc' => 'We may entrust the handling of personal information provided to third parties. When entrusting the handling of personal information, we will select a manager who has a sufficient level of personal information protection, provide guidance and supervision such as concluding a confidentiality agreement, and supervise appropriately.',
    ],
    'disclosure-info' => [
        'title' => 'Procedures for disclosure of personal information',
        'desc' => 'Regarding retained personal data, we will notify the purpose of use, disclosure, correction, addition, deletion, suspension of use or suspension of third party provision (“disclosure.”) by the method prescribed by company as follows. We will respond appropriately to the request within a reasonable range.',
        'l1' => 'Retained personal data subject to disclosure
        The personal information that is subject to disclosure is limited to personal information that the Company has the authority to disclose (“retained personal data”).',
        'l2' => 'Purpose of use of retained personal data
        All of the retained personal data of company will be used within the scope of the purpose of use described in (1) above.',
        'l3' => 'Disclosure request and complaint address
        Shimorenjaku 3-27-1, Mitaka city, Tokyo, 181-0013
        Attn: Personal Information Department, Acomo IncRequests for disclosure from the person or agent will be accepted by attaching the necessary documents to the specified “Request for Disclosure of Personal Information” and submitting it to the applicant by mail. When you mail the request, please use a method such as delivery record mail in order to confirm the delivery record. In addition, please write “Request for personal information” on the envelope in red color.',
        'l4' => 'Authorized personal information protection organization to which company belongs not applicable',
        'l5' => [
            'desc-1a' => 'Documents to be submitted when requesting disclosure',
            'desc-1b' => [
                'text' => '[ Identification ] Please submit one of the following documents.',
                'desc' => 'If you would like to make a request by a representative, please submit the following required documents in addition to the documents above.',
                'a' => 'Copy of Driver License',
                'b' => 'Copy of Residential Record',
                'c' => 'Copy of Health Insurance Card',
                'd' => 'Copy of Special Permanent Resident Certificate',
            ],
            'desc-2a' => '[ Proxy ] Please submit one of the following documents.',
            'desc-2b' => [
                'text' => 'If you would like to make a request by a representative, please submit the following required documents in addition to the documents above.',
                'a' => 'Power of attorney signed by the principal',
                'b' => 'Seal registration certificate of the seal stamped on the power of attorney',
                'c' => 'Any of the above mentioned documents of the representative',
            ],
        ],
        'l6' => [
            'title' => 'Fee',
            'desc' => 'Regarding disclosure requests, a fee of 500 yen (tax included) will be charged per request for notification of purpose of use and disclosure request. Please pay the fee by sending registered mail in cash, paying by transfer to a financial institution, or any other method specified by our company. When we confirm payment of the fee, we will respond to the disclosure request.',
        ],
        'l7' => [
            'title' => 'When we cannot respond to disclosure',
            'desc' => 'We will not be able to respond to disclosure requests if any of the following applies. In that case, we will notify you of the reason why we cannot respond to disclosure etc. In addition, we will charge a predetermined fee even if we cannot respond to disclosure.',
            'a' => 'If we cannot verify your identity, such as when the address on request or the address on identity verification document does not match the address registered with us.',
            'b' => 'When we cannot confirm the request as the proxy',
            'c' => 'If there are any defects in the required documents',
            'd' => 'When the personal information held by us cannot be identified because it does not match',
            'e' => 'When the request for disclosure does not correspond to the personal information subject to disclosure',
            'f' => 'When there is a risk of harming the life, health, property or other rights or interests of the person or a third party',
            'g' => 'When it is against the law and regulations',
        ],
    ],
    'cookie' => [
        'title' => 'Cookie',
        'desc' => 'Cookie is a mechanism that saves the usage history and input contents sent and received between the browser and server when a web page is used as a file on the user’s computer. The next time you access the same page, the cookie information will allow the page operator to change the display for each user. Websites can retrieve Cookie from your browser if you allow them to send and receive Cookie in your browser settings. In order to protect your privacy, your browser only sends the Cookie sent and received by the server of that website.',
        'l1' => 'About CookieCookie is a mechanism that saves the usage history and input contents sent and received between the browser and server when a web page is used as a file on the user’s computer. The next time you access the same page, the cookie information will allow the page operator to change the display for each user. Websites can retrieve Cookie from your browser if you allow them to send and receive Cookie in your browser settings. In order to protect your privacy, your browser only sends the Cookie sent and received by the server of that website.',
        'l2' => 'Using CookieWe use Cookie to protect your privacy, improve convenience, deliver ads, and collect statistical data. In addition, the distribution of advertisements may be stored and referred to via a third party based on the consignment to a third party.',
        'l3' => 'Deleting CookieThe user can select the settings for sending and receiving Cookie from “Allow all Cookie”, “Reject all Cookie”, “Notify user when Cookie is received”, etc. The setting method depends on the browser. Check the “Help” menu of your browser for Cookieettings. Please be aware that if you choose to reject all Cookie, you may be restricted in using various services on the Internet, such as being unable to receive services that require authentication. In addition, we may use an advertisement distribution service provided by a third party, and in connection with this, the third party acquires and uses Cookie from the browser of the user who browsed our site. There are cases. Cookie obtained by the third party will be handled in accordance with the third party’s privacy policy. The user may stop the use of Cookie by the third party for advertisement distribution by accessing the invalidation (opt-out) page of the advertisement distribution cookie provided on the website of the third party.',
    ],
    'access-log' => [
        'title' => 'Access Log',
        'desc' => 'Our service may collect access logs to improve convenience and create statistical data. We will not disclose access logs to third parties unless l',
    ],
    'revision' => [
        'title' => 'Revision',
        'desc' => 'The Company may change the “Privacy Policy” without prior notice in response to changes in social conditions, technological advances, changes in various environments, etc. If there are any changes, we will notify you by the method prescribed by our company.',
    ],
    'inquiry' => [
        'title' => 'Inquiry',
        'desc1' => 'If you have any questions or inquiries regarding Privacy Policy, please contact the following.',
        'desc2' => 'Shimorenjaku 3-27-1, Mitaka city, Tokyo, 181-0013',
        'desc3' => 'Attn: Personal Information Department, Acomo Inc',
        'desc4' => '050-5578-0667',
    ],
];