<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Footer Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the main footer widget
    |
    */

    'column_1' => [
        'title' => 'Quick Links',
        'line_1' => 'What is Touchless Check-in?',
        'line_2' => 'Functions',
        'line_3' => 'News Update',
        'line_4' => 'FAQ',
    ],
    'column_2' => [
        'title' => 'Contact',
        'line_1' => '3-27-1, Shimorenjaku, Miataka, Tokyo, Japan 181-0013',
        'line_2' => '+81-50-5578-0667',
        'line_3' => 'rsv@acomo-inc.co.jp',
        'line_4' => 'www.acomo-inc.co.jp',
    ],
];
