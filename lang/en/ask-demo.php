<?php

return [
  'title' => 'Ask Demo',
  'description' => 'Please fill in the required information in the form below and submit it. We will contact you at the email you entered.',
  'form' => [
    'label_name' => 'Name',
    'label_email' => 'Email',
    'label_phone' => 'Phone',
    'label_company' => 'Company',
    'label_message' => 'Message',
    'button_submit' => 'Submit',
    'status_sending' => 'Sending...',
    'message_success_title' => 'Thank you for contacting us.',
    'message_success' => 'We will contact you as soonest. Appreciate your patience.',
    'back' => 'Back'
  ],
];
