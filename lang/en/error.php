<?php

return [
    'form' => [
        'name_required' => 'The name field is required.',
        'email_required' => 'The email field is required.',
        'company_required' => 'The company field is required.',
        'phone_required' => 'The phone field is required.',
        'phone_length' => 'The phone field must be atleast 5 characters.',
    ],
];
