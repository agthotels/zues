<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the main nav widget
    |
    */

    'login' => 'ログイン',
    'try_it_free' => '無料アカウント登録',
    'solutions' => 'タッチレスソリューション',
    'functions' => '機能',
    'demo' => '動画',
    'document' => '資料',
    'faq' => 'FAQ',
    'inquiry' => '問い合わせ',

];
