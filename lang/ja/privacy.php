<?php

return [
    'title' => '個人情報の取り扱いについて',
    'desc' => '株式会社アコモ（以下「当社」といいます）は、皆様の個人情報について、これを保護することが企業としての社会的責任であると認識し、個人情報の保護に関する関係法令を遵守の上、以下のとおり適切に取り扱います。',
    'objectives' => [
        'title' => '利用目的',
        'desc' => '当社が取得する個人情報は、以下の目的のために利用いたします。',
        'l1' => '商品・サービスの提供及びご案内のため',
        'l2' => '商品・サービスの改善や新商品・サービスの開発等に役立てるため',
        'l3' => '商品・サービスをご利用いただく資格や本人確認のため',
        'l4' => '皆様との取引・契約・管理を適切かつ円滑に履行するため',
        'l5' => 'ご相談、お問い合わせに関する回答その他の対応のため',
        'l6' => '当社の商品・サービスに参画しているホテル・旅館等の宿泊施設または関連サービスを提供する会社への予約申込み、連絡その他当社の業務の遂行のため',
        'l7' => 'ダイレクトメールの発送等、当社及び提携会社等の第三者の商品・サービスに関するご案内のため',
        'l8' => 'アンケート調査、マーケティングデータの調査、統計、分析のため',
        'l9' => '決済サービス、物流サービスの提供のため',
        'l10' => '契約や法律等に基づく権利の行使や義務の履行のため',
        'l11' => '人事労務管理に関わる諸手続きを行うため',
        'l12' => '前各号に附帯関連する業務の実施のため',
        'l13' => 'その他、ご本人の同意を得た利用目的のためs',
    ],
    'acquisition' => [
        'title' => '個人情報の取得',
        'desc' => '皆様の個人情報を取得させていただく場合は、業務上必要な範囲において、適正かつ適法な手段により取得させていただきます。',
    ],
    'provision' => [
        'title' => '個人情報提供の任意性',
        'desc' => '個人情報を当社にご提供いただくか否かは、皆様ご自身の任意です。ただし、当社のサービスにおいて、それぞれ必要となる個人情報を提供いただけない場合や、ご提供いただいた個人情報に不備があった場合には、当社サービスを受けられない場合があります。',
    ],
    'management' => [
        'title' => '個人情報の管理',
        'desc' => '当社は、ご提供いただいた個人情報については、個人情報の保護に関する関係法令に則り、適切な保護・安全対策を実施し、個人情報の紛失、破壊、改ざん、漏えいの防止に努めます。',
    ],
    'personal-info' => [
        'title' => '個人情報の利用',
        'desc' => '当社は、皆様からご提供いただいた個人情報については、法令に別段の定めがある場合を除き、皆様にお知らせした利用目的以外の目的では利用いたしません。',
    ],
    'provision-personal-info' => [
        'title' => '個人情報の第三者提供',
        'desc' => '当社は、以下のいずれかに該当する場合を除き、個人情報を第三者に開示または提供いたしません。',
        'l1' => 'ご本人の同意がある場合',
        'l2' => '法令に基づく場合',
        'l3' => '人の生命、身体又は財産の保護のために必要であって、ご本人の同意を得ることが困難ある場合',
        'l4' => '国の機関もしくは地方公共団体またはその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合',
        'l5' => '裁判所の発する令状その他裁判所の判決、決定、命令に基づき開示する場合',
        'l6' => '検察、警察、監督官庁、税務署、消費者生活センター、弁護士会またはこれらに準じた権限を持つ機関からの適法・適正な手続きにより情報の照会があった場合',
        'l7' => 'その他、当社サービスにおいて個別に定める場合',
    ],
    'sensitive-info' => [
        'title' => '要配慮個人情報',
        'desc' => '当社は、皆様の要配慮個人情報（個人情報の保護に関する法律第２条第３項の定義に従います）については、あらかじめご本人の同意をいただいている場合や法令等に基づく場合等を除き、取得いたしません。',
    ],
    'consignment-info' => [
        'title' => '個人情報の委託について',
        'desc' => '当社は、ご提供いただいた個人情報の取り扱いを第三者に委託することがあります。個人情報の取り扱いを委託する場合には、十分な個人情報保護の水準を備える者を選び、秘密保持契約を締結するなど指導・監督を実施し、適切に監督いたします。',
    ],
    'disclosure-info' => [
        'title' => '個人情報の開示等の諸手続き',
        'desc' => '当社は、保有個人データについて、以下のとおり、当社所定の方法により、利用目的の通知、開示、訂正、追加、削除、利用停止または第三者提供停止（以下「開示等」といいます）の請求に合理的な範囲で適切に対応いたします。',
        'l1' => '開示等の対象となる保有個人データ
        開示等の対象となる個人情報は、当社が保有する個人情報のうち、当社が開示等の権限を有するもの（以下「保有個人データ」といいます）に限ります。s',
        'l2' => '保有個人データの利用目的
        当社のすべての保有個人データは、上記「１」記載の利用目的の範囲内で利用いたします。',
        'l3' => '開示等請求及び苦情の申出先
        〒181-0013　東京都三鷹市下連雀3−27−1
        株式会社アコモ　個人情報問い合わせ窓口ご本人または代理人からの開示等のご請求は、所定の「個人情報開示等請求書」に必要書類を添付の上、郵送により申出先にご提出いただくことによって受け付けます。請求書を郵送いただく際には、配達記録郵便や簡易書留郵便など、配達の記録が確認できる方法にてお願いいたします。なお、封筒に朱書きで「個人情報請求書在中」とお書き添えいただきますようお願いいたします。',
        'l4' => '当社が所属する認定個人情報保護団体
        該当なし',
        'l5' => [
            'desc-1a' => '開示等請求に際して提出いただく書面',
            'desc-1b' => [
                'text' => '',
                'desc' => '【ご本人様確認】 下記書類のうち、いずれかをご提出ください。',
                'a' => '運転免許証の写し',
                'b' => '住民票の写し',
                'c' => '健康保険証の被保険者証の写し',
                'd' => '在留カードまたは特別永住者証明書の写し',
            ],
            'desc-2a' => '【代理人様確認】 代理人によるご請求をご希望の方は、上記ご本人様確認書面に加えて、以下の書類をご提出ください。',
            'desc-2b' => [
                'text' => '',
                'a' => '委任状（ご本人様が捺印したもの）',
                'b' => '委任状に捺印した印鑑の印鑑登録証明書',
                'c' => '代理人についての上記【ご本人様確認】記載のいずれかの書面',
            ],
        ],
        'l6' => [
            'title' => '手数料',
            'desc' => '開示等のご請求に関しまして、利用目的の通知、ならびに開示請求については、請求１件につき手数料500円（税込）を頂戴いたします。手数料は、現金書留の送付、金融機関への振込みによる支払い、その他当社所定の方法によりお支払いください。手数料の払い込みを確認した時点で、開示等請求に対する対応をさせていただきます。',
        ],
        'l7' => [
            'title' => '開示等に応じられない場合',
            'desc' => '次のいずれかに該当する場合、開示等に応じかねます。その場合、開示等に応じられない理由を通知いたします。なお、開示等に応じられない場合についても所定の手数料を頂戴いたします。',
            'a' => '請求書に記載されている住所、本人確認書類に記載されている住所と当社に登録されている住所が一致しないときなど、本人確認ができない場合',
            'b' => '代理人によるご請求に際して、代理権が確認できない場合',
            'c' => '所定の提出書類に不備があった場合',
            'd' => '請求書の記載内容により、当社が保有する個人情報を特定できなかった場合',
            'e' => '開示のご請求の対象が開示対象個人情報に該当しない場合',
            'f' => '本人または第三者の生命、身体、財産その他の権利、利益を害するおそれがある場合',
            'g' => '法令に違反することとなる場合',
        ],
    ],
    'cookie' => [
        'title' => 'Cookie等について',
        'desc' => '当社は、ご本人のプライバシーの保護、利便性の向上、広告の配信、及び統計データの取得のため、Cookieを使用します。また、当社は、CookieやJavaScript等の技術を利用して、ご提供いただいた情報のうち個人が特定できない属性情報（組み合わせることによっても個人が特定できないものに限られます）や、当社サイト内におけるご本人の行動履歴（アクセスしたURL、コンテンツ、参照順等）を取得し、利便性の向上、広告の配信等に使用することがあります。ただし、Cookie及び行動履歴には個人情報は一切含まれません。 なお、当社は、ご本人がログインして当社サービスを利用した場合、個人を特定したうえで広告の配信・表示等をすることがあります。',
        'l1' => 'Cookieの概要についてCookieとは、ウェブページを利用した時に、ブラウザとサーバーとの間で送受信した利用履歴や入力内容などを、利用者のコンピュータにファイルとして保存しておく仕組みです。次回、同じページにアクセスすると、Cookieの情報を使って、ページの運営者は利用者ごとに表示を変えたりすることができます。利用者がブラウザの設定でCookieの送受信を許可している場合、ウェブサイトは、利用者のブラウザからCookieを取得できます。なお、利用者のブラウザは、プライバシー保護のため、そのウェブサイトのサーバーが送受信したCookieのみを送信します。',
        'l2' => 'Cookieの用途について当社は、ご本人のプライバシーの保護、利便性の向上、広告の配信、及び統計データの取得のため、Cookieを使用します。なお、広告の配信は第三者への委託に基づき、第三者を経由して、Cookieを保存し、参照する場合があります。',
        'l3' => 'Cookieの削除について利用者は、Cookieの送受信に関する設定を「すべてのCookieを許可する」、「すべてCookieを拒否する」、「Cookieを受信したらユーザーに通知する」などから選択できます。設定方法は、ブラウザにより異なります。 Cookieに関する設定方法は、お使いのブラウザの「ヘルプ」メニューでご確認ください。 すべてのCookieを拒否する設定を選択されますと、認証が必要なサービスを受けられなくなる等、インターネット上の各種サービスの利用上、制約を受ける場合がありますのでご注意ください。 また、当社では第三者が提供する広告配信サービスを利用する場合があり、これに関連して、当該第三者が当社のサイトを閲覧された利用者のブラウザからCookieを取得し、利用する場合があります。当該第三者によって取得されたCookieは、当該第三者のプライバシーポリシーに従って取り扱われます。利用者は、当該第三者のウェブサイト内に設けられた広告配信用Cookieの無効化（オプトアウト）ページにアクセスして、当該第三者によるCookieの広告配信への利用を停止することができます。',
    ],
    'access-log' => [
        'title' => 'アクセスログについて',
        'desc' => '当社の提供するサービスでは、利便性の向上及び統計データ作成の為にアクセスログを取得することがあります。法的に要求された場合を除き、第三者にアクセスログは公開しません。アクセスログの送信を拒否された場合は、当社サービスを正常に利用できない場合があります。',
    ],
    'revision' => [
        'title' => '改定',
        'desc' => '当社は、社会情勢の変化、技術の進歩、諸環境の変化等に応じ、事前の予告なく当該「個人情報の取り扱いについて」を変更することがあります。変更があった場合は、当社所定の方法により皆様へお知らせいたします。',
    ],
    'inquiry' => [
        'title' => 'お問い合わせ',
        'desc1' => '当社、当該「個人情報の取り扱いについて」についてご質問その他のお問い合わせは、以下までお問い合わせ',
        'desc2' => 'ください。',
        'desc3' => '〒181-0013　東京都三鷹市下連雀3-27-1',
        'desc4' => '株式会社アコモ　個人情報問い合わせ窓口 050-5578-0667',
    ],
];