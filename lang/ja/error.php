<?php

return [
    'form' => [
        'name_required' => '名前フィールドは必須です。',
        'email_required' => '電子メールフィールドは必須です。',
        'phone_required' => '電話番号フィールドは必須です。',
        'phone_length' => '電話フィールドは少なくとも 5 文字である必要があります',
        'company_required' => '会社フィールドは必須です。',
    ],
];
