<?php

return [
  'title' => 'デモを閲覧',
  'description' => '下記のフォームに必要事項を入力し送信してください。ご入力いただきましたメールアドレスにご連絡致します。',
  'form' => [
    'label_name' => 'お名前',
    'label_email' => 'Email',
    'label_phone' => '電話番号',
    'label_company' => '会社名',
    'button_submit' => '送信',
    'status_sending' => '送信...',
    'message_success_title' => 'お問い合わせいただきありがとうございました。',
    'message_success' => '担当者よりコンタクト致しますのでお待ちくださいますようお願
    い申し上げます。',
    'back' => '戻る'
  ],
];
