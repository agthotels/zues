<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PathController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\AskDemoController;
use App\Http\Controllers\WatchDemoController;
use App\Http\Controllers\DownloadDocumentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/f/{path}', [PathController::class, 'index'])->where('path', '.*');

Route::group(
  [
    'prefix' => '/{locale?}',
    'middleware' => 'detect_locale',
  ],
  function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/inquiry', [InquiryController::class, 'index']);
    Route::post('/inquiry', [InquiryController::class, 'store']);
    Route::get('/ask-demo', [AskDemoController::class, 'index']);
    Route::post('/ask-demo', [AskDemoController::class, 'store']);
    Route::get('/watch-demo', [WatchDemoController::class, 'index']);
    Route::post('/watch-demo', [WatchDemoController::class, 'store']);
    Route::get('/download-document', [DownloadDocumentController::class, 'index']);
    Route::post('/download-document', [DownloadDocumentController::class, 'sendEmailDownloadDocument']);
    Route::view('/terms-and-conditions', 'terms');
    Route::view('/privacy-policy', 'privacy');
  }
);
