<?php

namespace App\View\Components\Home;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Http;

class News extends Component
{
    private $data = [];
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $lang = last(request()->segments());
        if($lang == 'en') {
            $feedUrl = 'https://acomo-inc.co.jp/en/?rest_route=/wp/v2/posts&_embed&filter[orderby]=date&order=desc&per_page=4';
        } else {
            $feedUrl = 'https://acomo-inc.co.jp/?rest_route=/wp/v2/posts&_embed&filter[orderby]=date&order=desc&per_page=4';
        }
        $response = Http::get($feedUrl);
        $feed = $response->json();
        $news = [];

        foreach($feed as $item) {
            $news []= [
                'date' => date("Y-m-d", strtotime($item['date'])),
                'image' => $item['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['medium']['source_url'] ?? null,
                'title' => $item['title']['rendered'] ?? null,
                'description' => $this->getFirstParagraph($item) ?? null,
                'link' => $item['link'] ?? null,
            ];
        }

        $this->data['news'] = $news;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.home.news', $this->data);
    }

    private static function modifyDescriptionValue($item) {
        $removeDuplicateTitle = str_replace($item['title']['rendered'], "", $item['content']['rendered']);
        $removeReadMoreEN = str_replace("Read More &raquo;", "", $removeDuplicateTitle);
        return str_replace("もっと読む &raquo;", "", $removeReadMoreEN);
    }

    private static function getFirstParagraph($item) {
        preg_match('/<p>(.*?)<\/p>/s', $item['content']['rendered'], $match);
        return $match[0];
    }
}