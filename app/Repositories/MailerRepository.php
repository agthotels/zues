<?php

namespace App\Repositories;

use App\Email\Mailer as CampaignMonitor;
use App\Repositories\Interfaces\MailerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

use Exception;

class MailerRepository implements MailerInterface
{
    /**
     * @var mailer
     */
    private $mailer;

    public function __construct()
    {
        $this->mailer = new CampaignMonitor();
    }

    /**
     * Send Download Document 
     *
     * @param Request $request
     * @param [object] $data
     * @return mixed
     */
    public function sendDownloadDocument(Request $request, $data)
    {
        $this->mailer
            ->load('DownloadDocumentEmail')
            ->setEmailLang($data['lang'])
            ->withData($data)
            ->sendToUser(
                ['email' => $data['email']],
                ['yes', false]
            );
    }

    public function sendGenericEmail($data)
    {

        logger('sending to: '.$data['email']);
        $this->mailer
            ->load('GenericEmail')
            ->setEmailLang($data['lang'])
            ->withData($data)
            ->sendTo(
                ['email' => $data['email_recipient']],
                ['yes', false]
            );
    }
}
