<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Interface MailerInterface
 * Contains all reusable functions within the application
 * @package App\Repositories\Interfaces
 */
interface MailerInterface
{
    public function sendDownloadDocument(Request $request, $data);

    public function sendGenericEmail($data);
}
