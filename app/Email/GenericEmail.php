<?php

namespace App\Email;

class GenericEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        $emails = [
            'ja' => 'f9a2351e-de5e-435d-9626-512b954389cc',
            'en' => 'f9a2351e-de5e-435d-9626-512b954389cc'
        ];

        if (!isset($emails[$this->lang])) {
            return $emails['en'];
        }

        return $emails[$this->lang];
    }
}
