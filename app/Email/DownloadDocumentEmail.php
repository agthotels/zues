<?php

namespace App\Email;

class DownloadDocumentEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
    	$emails = [
    		'ja' => 'ff292a06-8bfc-45e1-b4fe-eea4522ff3b9',
    		'en' => 'd9b10a47-ab00-4a6e-8b30-4a86fa829e4a'
    	];

    	if (!isset($emails[$this->lang])) {
    		return $emails['en'];
    	}

    	return $emails[$this->lang];
    }
}