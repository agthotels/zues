<?php

namespace App\Email;

use CS_REST_Transactional_SmartEmail;

abstract class Email
{
    protected $files = [];
    /**
     * The CM api key.
     *
     * @var string
     */
    protected $apiKey;
    protected $lang;

    /**
     * Any applicable data for the email.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Create a new WelcomeEmail instance.
     *
     * @param string|null $apiKey
     */
    public function __construct($apiKey = null)
    {
        $this->apiKey = $apiKey ?: config('app.campaign_monitor_key');
        $this->lang = 'ja';
    }

    public function setAttachment(array $data)
    {

        foreach ($data as $key => $value) {
            $this->files[] = $value;
        }

        return $this;
    }

    /**
     * Set the email data.
     *
     * @param  array $data
     * @return $this
     */
    public function withData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Send an transactional email.
     *
     * @param  $user
     * @return \CS_REST_Wrapper_Result
     */
    public function sendTo($user, array $extra_params = array('no', false))
    {
        $mailer = $this->newTransaction();

        $message = [
            'To'   => isset($user['email']) ?  $user['email'] : NULL,
            'BCC' => isset($user['bcc']) ?  $user['bcc'] : NULL,
            'Data' => $this->getData($user),
            "Attachments" => $this->files
        ];

        # Add consent to track value
        $consent_to_track = 'no'; # Valid: 'yes', 'no', 'unchanged'

        # Send the message and save the response
        $mailer->send($message, $consent_to_track);
    }

    /**
     * Send an transactional email.
     *
     * @param  $user
     * @return \CS_REST_Wrapper_Result
     */
    public function sendToUser($user)
    {
        $this->apiKey;
        $mailer = $this->newTransaction();
        $message = [
            "To" => $user['email'],
            "Data" => [
                'link' => 'https://static.checkinherenow.com/prd-landing/pdf/Touchless-Checkin-Service-v2304-for-DL.pdf',
            ],
        ];

        # Add consent to track value
        $consent_to_track = 'no'; # Valid: 'yes', 'no', 'unchanged'

        # Send the message and save the response
        $result = $mailer->send($message, $consent_to_track);
    }

    /**
     * Get the data for the email.
     *
     * @param  $user
     * @return array
     */
    public function getData($user)
    {
        if (method_exists($this, 'variables')) {
            return call_user_func_array(
                [$this, 'variables'],
                array_merge(compact('user'), $this->data)
            );
        }

        return $this->data;
    }

    /**
     * Create a new CM smart email instance.
     *
     * @return CS_REST_Transactional_SmartEmail
     */
    protected function newTransaction()
    {
        return new CS_REST_Transactional_SmartEmail(
            $this->getEmailId(),
            ['api_key' => $this->apiKey]
        );
    }

    public function setEmailLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * Get the email id.
     *
     * @return string
     */
    protected abstract function getEmailId();
}

class Mailer
{
    public function load($className)
    {
        $class = 'App\\Email\\' . $className;
        return new $class();
    }
}
