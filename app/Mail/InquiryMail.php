<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Address;

class InquiryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email, $name, $_subject, $phone, $company, $_message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        string $email,
        string $name = 'unknown',
        string $subject = '<Inquiry> Touchless Check-in for Hotels',
        string $company = 'none',
        string $phone = 'none',
        string $message = 'none',
    )
    {
        $this->email = $email;
        $this->name = $name;
        $this->_subject = $subject;
        $this->phone = $phone;
        $this->company = $company;
        $this->_message = $message;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        $senderAddress = config('mail.from.address');
        $senderName = config('mail.from.name');

        return new Envelope(
            from: new Address($senderAddress, $senderName),
            replyTo: [ new Address($this->email, $this->name), ],
            subject: $this->_subject,
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'emails.generic-inquiry',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
