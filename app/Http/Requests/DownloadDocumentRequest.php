<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DownloadDocumentRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required',
			'company' => 'required',
			'phone' => 'required',
			'email' => 'required|string|email|max:255',
			'g-recaptcha-response' => ['recaptcha'],
		];
	}

	public function messages()
	{
		return [
			"name.required" => __('error.form.name_required'),
			"email.required" => __('error.form.email_required'),
			"phone.required" => __('error.form.phone_required'),
			"company.required" => __('error.form.company_required')
		];
	}
}
