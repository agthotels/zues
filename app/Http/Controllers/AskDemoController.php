<?php

namespace App\Http\Controllers;

use App\Http\Requests\AskDemoRequest;
use App\Repositories\Interfaces\MailerInterface;

class AskDemoController extends Controller
{
    /**
     * @var MailerInterface
     */
    private $mailerInterface;

    public function __construct(
        MailerInterface $mailerInterface,
    ) {
        $this->mailerInterface = $mailerInterface;
    }

    public function index()
    {
        return view('ask-demo');
    }

    public function store(AskDemoRequest $request)
    {
        $data = $request->all();

        $data['subject'] = "<Ask Demo> Touchless Check-in for Hotels";

        $recipients = explode(',', config('mail.to.address'));
   
        $data['lang'] = app()->getLocale();
        foreach ($recipients as $recipient) {
            $data["email_recipient"] = $recipient;
            $this->mailerInterface->sendGenericEmail($data);
        }

        $data['email_sent'] = 1;

        return view('ask-demo', $data);
    }
}
