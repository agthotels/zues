<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\MailerInterface;
use App\Http\Requests\DownloadDocumentRequest;

class DownloadDocumentController extends Controller
{


    /**
     * @var MailerInterface
     */
    private $mailerInterface;

    public function __construct(
        MailerInterface $mailerInterface,
    ) {
        $this->mailerInterface = $mailerInterface;
    }

    public function index(Request $request)
    {
        return view('download-document');
    }

    public function sendEmailDownloadDocument(DownloadDocumentRequest $request)
    {
        $data = $request->all();

        // get lang
        $segments = $request->segments();

        $data['lang'] = $segments[0];

        $mailerApiKey = $this->mailerInterface->sendDownloadDocument($request, $data);

        $data['subject'] = "<Get Document> Touchless Check-in for Hotels";

        $recipients = explode(',', config('mail.to.address'));

        $data['lang'] = app()->getLocale();
        foreach ($recipients as $recipient) {
            $data["email_recipient"] = $recipient;
            $this->mailerInterface->sendGenericEmail($data);
        }


        $data['email_sent'] = 1;

        return view('download-document', $data);
    }
}
