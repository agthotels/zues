<?php

namespace App\Http\Controllers;

class PathController extends Controller
{
    public function index() {
        $requestUri = request()->getRequestUri();
        $cleanUrl = preg_replace('/^\/f/', '', $requestUri);
        $appUrl = config('app.url').$cleanUrl;
        return view('redirect', ['app_url' => $appUrl]);
    }
}
