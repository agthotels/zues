<?php

namespace App\Http\Controllers;

use App\Http\Requests\WatchDemoRequest;
use App\Repositories\Interfaces\MailerInterface;

class WatchDemoController extends Controller
{
    /**
     * @var MailerInterface
     */
    private $mailerInterface;

    public function __construct(
        MailerInterface $mailerInterface,
    ) {
        $this->mailerInterface = $mailerInterface;
    }
    public function index()
    {
        return view('watch-demo');
    }

    public function store(WatchDemoRequest $request)
    {
        $data = [];

        $data = $request->all();

        $data['subject'] = "<Watch Demo> Touchless Check-in for Hotels";

        $recipients = explode(',', config('mail.to.address'));
        $data['lang'] = app()->getLocale();
        foreach ($recipients as $recipient) {
            $data["email_recipient"] = $recipient;
            $this->mailerInterface->sendGenericEmail($data);
        }

        $data['email_sent'] = 1;

        return view('watch-demo', $data);
    }
}
