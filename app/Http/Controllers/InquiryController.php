<?php

namespace App\Http\Controllers;

use App\Http\Requests\InquiryRequest;
use App\Repositories\Interfaces\MailerInterface;

class InquiryController extends Controller
{
    /**
     * @var MailerInterface
     */
    private $mailerInterface;

    public function __construct(
        MailerInterface $mailerInterface,
    ) {
        $this->mailerInterface = $mailerInterface;
    }
    public function index()
    {
        return view('inquiry');
    }

    public function store(InquiryRequest $request)
    {
        $data = $request->all();
        $data['subject'] = "<Inquiry> Touchless Check-in for Hotels";
        $recipients = explode(',', config('mail.to.address'));
        $data['lang'] = app()->getLocale();

        foreach ($recipients as $recipient) {
            $data["email_recipient"] = $recipient;
            $this->mailerInterface->sendGenericEmail($data);
        }

        $data['email_sent'] = 1;
        return view('inquiry', $data);
    }
}
