<?php

function app_url($url)
{
    $appUrl = \Illuminate\Support\Facades\Config::get('app.url');
    $parts = [ rtrim($appUrl, '/') ];
    if ($url) $parts[] = $url;

    return implode('/', $parts);
}