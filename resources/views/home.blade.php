<!DOCTYPE html>
<html lang="en-US">

<head>
  <x-header-root />
</head>

<body class="page-template page-template-page-home page-template-page-home-php page page-id-13 page-parent" data-spy="scroll" data-target=".fixed-top">

  <!-- Top Nav -->
  <x-nav />
  <!-- /Top Nav -->

  <!-- Hero Block -->
  <x-home.hero />
  <!-- /Hero Block -->

  <!-- What is Touchless Check-in -->
  <div id="services" class="blue-container cards-1 py-3">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.touchless_info.title') }}</h2>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-lg-4 col-md-6 col-12 mb-3">
          <div class="service-card">
            <div>
              <div class="services-img">
                <img class="img-fluid mb-4 wow animate__zoomIn" data-wow-duration="2s" src="{{ asset('images/touchless-info/no-kiosk.jpg') }}" alt="">
              </div>
              <div class="wow animate__slideInUp" data-wow-duration="2s">
                <h4>{{ __('home.touchless_info.point_1') }}</h4>
                <p>{{ __('home.touchless_info.point_1_description') }}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-12 mb-3">
          <div class="service-card">
            <div>
              <div class="services-img">
                <img class="img-fluid mb-4 wow animate__zoomIn" data-wow-duration="2s" src="{{ asset('images/touchless-info/smartphone.jpg') }}" alt="">
              </div>
              <div class="wow animate__slideInUp" data-wow-duration="2s">
                <h4>{{ __('home.touchless_info.point_2') }}</h4>
                <p>{{ __('home.touchless_info.point_2_description') }}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-12 mb-3">
          <div class="service-card">
            <div>
              <div class="services-img">
                <img class="img-fluid mb-4 wow animate__zoomIn" data-wow-duration="2s" src="{{ asset('images/touchless-info/cost-effective.jpg') }}" alt="">
              </div>
              <div class="wow animate__slideInUp" data-wow-duration="2s">
                <h4>{{ __('home.touchless_info.point_3') }}</h4>
                <p>{{ __('home.touchless_info.point_3_description') }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /What is Touchless Check-in -->

  <!-- Check in Process -->
  <div id="checkInProcess" class="blue-container cards-1 py-3">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.touchless_process.title') }}</h2>
        </div>
      </div>
      <div class="row mb-3 justify-content-center">
        <div class="col-lg-3 col-md-12 col-12 mb-3 mb-lg-0 process-wrapper">
          <div>
            <div class="process-image mb-3">
              <img class="img-fluid mb-4" src="{{ asset('images/touchless-process/image-1.jpg') }}" alt="" />
            </div>
            <div class="px-2">
              <h4>{{ __('home.touchless_process.step_1') }}</h4>
              <p>{{ __('home.touchless_process.step_1_point_1') }}</p>
              <p>{{ __('home.touchless_process.step_1_point_2') }}</p>
              <p>{{ __('home.touchless_process.step_1_point_3') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-1 d-lg-block d-none align-self-center px-4 custom-arrow">
          <img class="img-fluid" src="{{ asset('images/touchless-process/triangle-right-arrow.png') }}" />
        </div>
        <div class="col-lg-3 col-md-12 col-12 mb-3 mb-lg-0 process-wrapper">
          <div>
            <div class="process-image mb-3">
              <img class="img-fluid mb-4" src="{{ asset('images/touchless-process/image-2.jpg') }}" alt="" />
            </div>
            <div class="px-2">
              <h4>{{ __('home.touchless_process.step_2') }}</h4>
              <p>{{ __('home.touchless_process.step_2_point_1') }}</p>
              <p>{{ __('home.touchless_process.step_2_point_2') }}</p>
              <p>{{ __('home.touchless_process.step_2_point_3') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-1 d-lg-block d-none align-self-center px-4 custom-arrow">
          <img class="img-fluid" src="{{ asset('images/touchless-process/triangle-right-arrow.png') }}" />
        </div>
        <div class="col-lg-3 col-md-12 col-12 mb-3 mb-lg-0 process-wrapper">
          <div>
            <div class="process-image mb-3">
              <img class="img-fluid mb-4" src="{{ asset('images/touchless-process/new-picture-for-process-during-stay-1-min.jpg') }}" alt="" />
            </div>
            <div class="px-2">
              <h4>{{ __('home.touchless_process.step_4') }}</h4>
              <p>{{ __('home.touchless_process.step_4_point_1') }}</p>
              <p>{{ __('home.touchless_process.step_4_point_2') }}</p>
              <p>{{ __('home.touchless_process.step_4_point_3') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-1 d-lg-block d-none align-self-center px-4 custom-arrow">
          <img class="img-fluid" src="{{ asset('images/touchless-process/triangle-right-arrow.png') }}" />
        </div>
        <div class="col-lg-3 col-md-12 col-12 mb-3 mb-lg-0 process-wrapper">
          <div>
            <div class="process-image mb-3">
              <img class="img-fluid mb-4" src="{{ asset('images/touchless-process/image-3.jpg') }}" alt="" />
            </div>
            <div class="px-2">
              <h4>{{ __('home.touchless_process.step_3') }}</h4>
              <p>{{ __('home.touchless_process.step_3_point_1') }}</p>
              <p>{{ __('home.touchless_process.step_3_point_2') }}</p>
              <p>{{ __('home.touchless_process.step_3_point_3') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Check in Process -->

  <!-- Solutions -->
  <div id="solutions" class="cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2>{{ __('home.solutions.title') }}</h2>
        </div>
      </div>
      <div class="row mb-3 pt-5">
        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_1') }}</h4>
              <p>{{ __('home.solutions.solution_1_description') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_2') }}</h4>
              <p>{{ __('home.solutions.solution_2_description') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_3') }}</h4>
              <p>{{ __('home.solutions.solution_3_description') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_4') }}</h4>
              <p>{{ __('home.solutions.solution_4_description') }}</p>
            </div>
          </div>
        </div>


        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_7') }}</h4>
              <p>{{ __('home.solutions.solution_7_description') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_8') }}</h4>
              <p>{{ __('home.solutions.solution_8_description') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_5') }}</h4>
              <p>{{ __('home.solutions.solution_5_description') }}</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="function-card">
            <div class="function-img-con">
              <img src="{{ asset('images/dummy-icon.jpg') }}" alt="" />
            </div>
            <div class="wow animate__fadeIn" data-wow-duration="1s">
              <h4 class="card-title">{{ __('home.solutions.solution_6') }}</h4>
              <p>{{ __('home.solutions.solution_6_description') }}</p>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div class="col-lg-12 mb-md-5">
          <h3>{{ __('home.solutions.checkin_out') }}</h3>
        </div>
        <div class="row mb-3">
          <div class="col-lg-6 col-12 mb-3">
            <div class="custom-align-center text-justify">
              {{ __('home.solutions.checkin_out_description') }}
            </div>
          </div>
          <div class="col-lg-6 col-12">
            <div id="metaslider-id-738" style="width: 100%;" class="ml-slider-3-20-3 metaslider metaslider-flex metaslider-738 ml-slider">
              <div id="metaslider_container_738">
                <div id="metaslider_738">
                  <ul aria-live="polite" class="slides">
                    <li style="display: block; width: 100%;" class="slide-741 ms-image">
                      <img width="2560" height="1703" src="{{ asset('images/20210826-Scanner-Picture-DSC04334-scaled.jpg') }}" class="slider-738 slide-741" alt="" rel="" title="SONY DSC" style="margin-top: 0.37464488636364%" srcset="{{ asset('images/20210826-Scanner-Picture-DSC04334-scaled.jpg') }}, {{ asset('images/20210826-Scanner-Picture-DSC04334-300x200.jpg') }}, {{ asset('images/20210826-Scanner-Picture-DSC04334-2048x1362.jpg') }}" sizes="(max-width: 2560px) 100vw, 2560px" />
                    </li>
                    <li style="display: none; width: 100%;" class="slide-742 ms-image">
                      <img width="2560" height="1703" src="{{ asset('images/20210826-Scanner-Picture-DSC04335-2048x1362.jpg') }}" sizes="(max-width: 2560px) 100vw, 2560px" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="functions" class="cards-1">
        <div class="col-lg-12">
          <h3>{{ __('home.solutions.functions') }}</h3>
        </div>
      </div>
      <div class="row p-3">
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_1') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_2') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_3') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_4') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_5') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_6') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_7') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_8') }}</div>
        <div class="col-lg-4 col-md-6 col-12 mb-3 custom-bullet">{{ __('home.solutions.functions_9') }}</div>
      </div>
      <div class="row mt-5">
        <div class="col-12">
          <a class="btn custom-lg-btn" href="{{ url($locale.'/ask-demo') }}">
            {{ __('home.solutions.button_demo') }}
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- /Solutions -->

  <!-- News Update -->
  <x-home.news />
  <!-- /News Update -->

  <!-- Watch Online Demo -->
  <div id="watchDemo" class="cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.demo.title') }}</h2>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-lg-6 col-12 mb-3">
          <div class="custom-align-center text-justify">
            {{ __('home.demo.description') }}
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <img class="img-fluid w-100" src="{{ asset('images/office-1.jpg') }}" alt="" />
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12">
          <a class="btn custom-lg-btn" href="{{ url($locale.'/watch-demo') }}">
            {{ __('home.demo.button_watch') }}
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- /Watch Online Demo -->

  <!-- Read Document -->
  <div id="readDocument" class="blue-container cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row  mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.document.title') }}</h2>
        </div>
      </div>
      <div class="row  mb-3">
        <div class="col-lg-6 col-12 mb-3">
          <img class="img-fluid w-100" src="{{ asset('images/office-2.jpg') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12">
          <div class="custom-align-center text-justify">
            {{ __('home.document.description') }}
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12">
          <a class="btn custom-lg-btn" href="{{ url($locale.'/download-document') }}">
              {{ __('home.document.button_get') }}
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- /Read Document -->

  <!-- Price -->
  <div id="price" class="cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.price.title') }}</h2>
        </div>
      </div>
      <div class="row mb-md-0 mb-3">
        <div class="col-sm-5 col-12">
          <div class="custom-card w-100 mb-lg-5">
            <div class="card-title border-bottom pb-3">
              <h3>{{ __('home.price.price_1') }}</h3>
            </div>
            <div class="row mt-3 mb-3">
              <div class="col-12">
                <p style="text-align: center;" data-ccp-props="{&quot;335551550&quot;:2,&quot;335551620&quot;:2,&quot;335559683&quot;:0,&quot;335559685&quot;:0,&quot;335559731&quot;:0,&quot;335559737&quot;:0,&quot;335562764&quot;:2,&quot;335562765&quot;:1,&quot;335562766&quot;:4,&quot;335562767&quot;:0,&quot;335562768&quot;:4,&quot;335562769&quot;:0}">
                  <span data-usefontface="false" data-contrast="none">{{ __('home.price.from') }}</span>
                  <span data-usefontface="false" data-contrast="none">
                    <strong style="font-size: 1.5rem;">{{ __('home.price.price_1_amount') }}</strong>
                    {{ __('home.price.price_1_timing') }}
                  </span>​
                </p>
                <p style="text-align: center;" data-ccp-props="{&quot;335551550&quot;:2,&quot;335551620&quot;:2,&quot;335559683&quot;:0,&quot;335559685&quot;:0,&quot;335559731&quot;:0,&quot;335559737&quot;:0,&quot;335562764&quot;:2,&quot;335562765&quot;:1,&quot;335562766&quot;:4,&quot;335562767&quot;:0,&quot;335562768&quot;:4,&quot;335562769&quot;:0}">
                  <span data-usefontface="false" data-contrast="none">
                    {{ __('home.price.price_1_description') }}
                  </span>​
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2 col-12 text-center">
          <div class="custom-align-center price-add">+</div>
        </div>
        <div class="col-sm-5 col-12">
          <div class="custom-card w-100 mb-lg-5">
            <div class="card-title border-bottom pb-3">
              <h3>{{ __('home.price.price_2') }}</h3>
            </div>
            <div class="row mt-3 mb-3">
              <div class="col-12">
                <p style="text-align: center;" data-ccp-props="{&quot;335551550&quot;:2,&quot;335551620&quot;:2,&quot;335559683&quot;:0,&quot;335559685&quot;:0,&quot;335559731&quot;:0,&quot;335559737&quot;:0,&quot;335562764&quot;:2,&quot;335562765&quot;:1,&quot;335562766&quot;:4,&quot;335562767&quot;:0,&quot;335562768&quot;:4,&quot;335562769&quot;:0}">
                  <span data-usefontface="false" data-contrast="none">{{ __('home.price.from') }}</span>
                  <strong style="font-size: 1.5rem;">{{ __('home.price.price_2_amount') }}</strong>
                  <span data-usefontface="false" data-contrast="none">{{ __('home.price.price_2_timing') }}</span>​
                </p>
                <p style="text-align: center;" data-ccp-props="{&quot;335551550&quot;:2,&quot;335551620&quot;:2,&quot;335559683&quot;:0,&quot;335559685&quot;:0,&quot;335559731&quot;:0,&quot;335559737&quot;:0,&quot;335562764&quot;:2,&quot;335562765&quot;:1,&quot;335562766&quot;:4,&quot;335562767&quot;:0,&quot;335562768&quot;:4,&quot;335562769&quot;:0}">
                  <span data-usefontface="false" data-contrast="none">
                    {{ __('home.price.price_2_description') }}
                  </span>​
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Price -->

  <!-- Inquiry -->
  <div id="inquiry" class="blue-container cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.inquiry.title') }}</h2>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-12">
          <div class="custom-align-center text-justify">
            {{ __('home.inquiry.description') }}
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12">
          <a class="btn custom-lg-btn" href="{{ url($locale.'/inquiry') }}">
            {{ __('home.inquiry.button') }}
          </a>
        </div>
      </div>
    </div>
  </div>

  <!-- Testimonial -->
  <div id="testimonial" class="cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.testimonial.title') }}</h2>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-12">
          <div class="row">
            <div class="col-md-4 col-12 news-image">
              <img class="img-fluid wow animate__fadeIn" data-wow-duration="2s" src="{{ asset('images/Picture4.png') }}" alt="" />
            </div>
            <div class="col-md-8 col-12 d-md-flex">
              <div class="text-lg-left text-justify mt-3 mb-3">
                <div class="mt-3 wow animate__slideInUp" data-wow-duration="2s">
                  <h4 class="card-title"></h4>
                  <p class="news-ellipsis">
                    {{ __('home.testimonial.description') }}
                  </p>
                </div>
                <a target="_blank" href="https://acomo-inc.co.jp/20210215pressreleasejp/" class="btn read-more">
                  {{ __('home.testimonial.button') }}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Testimonial -->

  <!-- Partners -->
  <div id="partners" class="blue-container cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.partners.title') }}</h2>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/Picture6.png') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/Picture5.png') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/Picture7.png') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/dk-dots.png') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/staysee.png') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/PMS-Neppann-1.png') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/navc.png') }}" alt="" />
        </div>
        <div class="col-lg-6 col-12 mb-5">
          <img class="img-fluid wow animate__zoomIn" src="{{ asset('images/njc.png') }}" alt="" />
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-12">
          <p style="text-align: center;">{{ __('home.partners.description_1') }}</p>
          <p style="text-align: center;">{{ __('home.partners.description_2') }}</p>
        </div>
      </div>
    </div>
  </div>
  <!-- /Partners -->

  <!-- FAQ -->
  <div id="faq" class="cards-1 pb-5 pt-5">
    <div class="container">
      <div class="row mb-3">
        <div class="col-lg-12">
          <h2>{{ __('home.faq.title') }}</h2>
        </div>
        <div class="col-12 accordion">
          <div class="cardac">
            <div class="card-headerac">
              <h4>{{ __('home.faq.question_1') }}</h4>
              <span class="fa fa-minus"></span>
            </div>
            <div class="card-bodyac">
              <p>{{ __('home.faq.answer_1') }}</p>
            </div>
          </div>
          <div class="cardac">
            <div class="card-headerac">
              <h4>{{ __('home.faq.question_2') }}</h4>
              <span class="fa fa-minus"></span>
            </div>
            <div class="card-bodyac">
              <p>{{ __('home.faq.answer_2') }}</p>
            </div>
          </div>
          <div class="cardac">
            <div class="card-headerac">
              <h4>{{ __('home.faq.question_3') }}</h4>
              <span class="fa fa-minus"></span>
            </div>
            <div class="card-bodyac">
              <p>{{ __('home.faq.answer_3') }}</p>
            </div>
          </div>
          <div class="cardac">
            <div class="card-headerac">
              <h4>{{ __('home.faq.question_4') }}</h4>
              <span class="fa fa-minus"></span>
            </div>
            <div class="card-bodyac">
              <p>{{ __('home.faq.answer_4') }}</p>
            </div>
          </div>
          <div class="cardac">
            <div class="card-headerac">
              <h4>{{ __('home.faq.question_5') }}</h4>
              <span class="fa fa-minus"></span>
            </div>
            <div class="card-bodyac">
              <p>{{ __('home.faq.answer_5') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /FAQ -->

  <x-footer />

  <x-footer-script />

  <link rel='stylesheet' id='metaslider-flex-slider-css' href='{{ asset('ml-slider/assets/sliders/flexslider/flexslider.css') }}' type='text/css' media='all' property='stylesheet' />
  <link rel='stylesheet' id='metaslider-public-css' href='{{ asset('ml-slider/assets/metaslider/public.css') }}' type='text/css' media='all' property='stylesheet' />
  <script type='text/javascript' src='{{ asset('ml-slider/assets/sliders/flexslider/jquery.flexslider.min.js') }}'></script>
  <script type='text/javascript'>
    var metaslider_452 = function($) {
      $('#metaslider_452').addClass('flexslider');
      $('#metaslider_452').flexslider({
        slideshowSpeed: 5000,
        animation: "fade",
        controlNav: true,
        directionNav: false,
        pauseOnHover: true,
        direction: "horizontal",
        reverse: false,
        animationSpeed: 600,
        prevText: "Previous",
        nextText: "Next",
        fadeFirstSlide: false,
        slideshow: true
      });
      $(document).trigger('metaslider/initialized', '#metaslider_452');
    };
    var timer_metaslider_452 = function() {
      var slider = !window.jQuery ? window.setTimeout(timer_metaslider_452, 100) : !jQuery.isReady ? window.setTimeout(timer_metaslider_452, 1) : metaslider_452(window.jQuery);
    };
    timer_metaslider_452();
    var metaslider_738 = function($) {
      $('#metaslider_738').addClass('flexslider');
      $('#metaslider_738').flexslider({
        slideshowSpeed: 5000,
        animation: "fade",
        controlNav: true,
        directionNav: false,
        pauseOnHover: true,
        direction: "horizontal",
        reverse: false,
        animationSpeed: 600,
        prevText: "Previous",
        nextText: "Next",
        fadeFirstSlide: false,
        slideshow: true
      });
      $(document).trigger('metaslider/initialized', '#metaslider_738');
    };
    var timer_metaslider_738 = function() {
      var slider = !window.jQuery ? window.setTimeout(timer_metaslider_738, 100) : !jQuery.isReady ? window.setTimeout(timer_metaslider_738, 1) : metaslider_738(window.jQuery);
    };
    timer_metaslider_738();
  </script>

</body>

</html>