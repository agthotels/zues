<!DOCTYPE html>
<html lang="en">
<body>
  <div style="width:100%; padding: 30px; background: #e5e5e5">
    <div style="width: 600px; padding: 30px; background: white; border: 1px solid #a5a5a5; margin: auto;">
      <table style="width: 100%; border-collapse: collapse">
        <tr>
          <td><b>Name</b></td>
        </tr>
        <tr>
          <td>{{ $name }}</td>
        </tr>
        <tr>
          <td><hr style="border-width: 1px; margin: 15px 0" /></td>
        </tr>
        <tr>
          <td><b>Email</b></td>
        </tr>
        <tr>
          <td>{{ $email }}</td>
        </tr>
        <tr>
          <td><hr style="border-width: 1px; margin: 15px 0" /></td>
        </tr>
        <tr>
          <td><b>Phone</b></td>
        </tr>
        <tr>
          <td>{{ $phone }}</td>
        </tr>
        <tr>
          <td><hr style="border-width: 1px; margin: 15px 0" /></td>
        </tr>
        <tr>
          <td><b>Company</b></td>
        </tr>
        <tr>
          <td>{{ $company }}</td>
        </tr>
        <tr>
          <td><hr style="border-width: 1px; margin: 15px 0" /></td>
        </tr>
        <tr>
          <td><b>Message</b></td>
        </tr>
        <tr>
          <td>{{ $_message }}</td>
        </tr>
      </table>
    </div>
  </div>
</body>
</html>
