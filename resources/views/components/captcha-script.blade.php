<script type="text/javascript">
    var query_form = document.querySelector('#inquiry_form');
    var inputs = document.querySelectorAll('#inquiry_form input');
    var message_box = document.querySelector('#inquiry_form textarea');
    var submit_button = document.querySelector('#inquiry_form button.wpforms-submit');

    function prevent_default(e) {
        e.preventDefault();
    }

    for (var n = 0; n < inputs.length; n++) inputs[n].setAttribute('disabled', true);
    if (message_box) message_box.setAttribute('disabled', true);
    submit_button.setAttribute('disabled', true);
    query_form.addEventListener('submit', prevent_default);

    function callbackThen(response) {
        response.json().then(function(data) {
            if (data.success && data.score >= 0.6) {
                console.log('reCAPTCHA connection success');
                for (var n = 0; n < inputs.length; n++) inputs[n].removeAttribute('disabled');
                if (message_box) message_box.removeAttribute('disabled');
                submit_button.removeAttribute('disabled');
                query_form.removeEventListener('submit', prevent_default);
            } else {
                alert('Loading reCAPTCHA failed')
            }
        });
    }

    function callbackCatch(error) {
        alert('Loading reCAPTCHA failed');
    }
</script>
{!! htmlScriptTagJsApi([
'callback_then' => 'callbackThen',
'callback_catch' => 'callbackCatch',
]) !!}