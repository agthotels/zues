<div class="spinner-wrapper">
  <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
  <!-- Text Logo - Use this if you don't have a graphic logo -->
  <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Check In Here Now</a> -->
  <!-- Image Logo -->
  <a class="navbar-brand logo-image" href="{{ url($locale) }}">
    <img src="{{ asset('images/Acomo-Logo-Hi-res-1024x232.png') }}" alt="" />
  </a>
  <!-- Mobile Menu Toggle Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-awesome fas fa-bars"></span>
    <span class="navbar-toggler-awesome fas fa-times"></span>
  </button>
  <!-- end of mobile menu toggle button -->
  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav ml-auto upper-navbar mb-1 align-items-center">
      <li class="nav-item">
        <a class="nav-link page-scroll" href="https://www.facebook.com/acomo.inc/" target="_blank">
          <i class="fab fa-facebook-f fa-stack-1x"></i>
        </a>
      </li>
      <li class="nav-item border-right">
        <a class="nav-link page-scroll" href="{{ app_url('login') }}">{{ __('nav.login') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll btn-tif" href="{{ app_url('email-verification') }}">{{ __('nav.try_it_free') }}</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link page-scroll" href="/ja" language='japanese' class="active">
          <img src="{{ asset('images/japan-flag-icon-free-download.jpg') }}" /> JP <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="/en" language='english'>
          <img src="{{ asset('images/us-flag-icon-free-download.png') }}" /> EN 
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#solutions">{{ __('nav.solutions') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#functions">{{ __('nav.functions') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#watchDemo">{{ __('nav.demo') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#readDocument">{{ __('nav.document') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#faq">{{ __('nav.faq') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#inquiry">{{ __('nav.inquiry') }}</a>
      </li>
    </ul>
  </div>
</nav>
<!-- end of navbar -->