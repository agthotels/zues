<div id="news" class="blue-container cards-1 pb-5 pt-5">
  <div class="container">
    <div class="row mb-3">
      <div class="col-lg-12">
        <h2>{{ __('home.news.title') }}</h2>
      </div>
    </div>
    <div class="row mb-3">
      @foreach ($news as $item)
      <div class="col-lg-3">
        <div class="text-lg-left text-justify mt-3 mb-3">
          <div class="news-image">
            <img class="img-fluid wow animate__fadeIn" data-wow-duration="2s" src="{{ $item['image'] }}" alt="" />
          </div>
          <div class="mt-3 wow animate__slideInUp" data-wow-duration="2s">
            <h4 class="card-title">{{ $item['title'] }}</h4>
            <h6>{{ $item['date'] }}</h6>
            <p class="news-ellipsis">
              {!!$item['description']!!}
            </p>
          </div>
          <a target="_blank" href="{{ $item['link'] }}" class="btn read-more">
            {{ __('home.news.read_more') }}
          </a>
        </div>
      </div>
      @endforeach
      <div class="col-12 text-right">
        <a target="_blank" href="https://acomo-inc.co.jp/en/#news" class="btn btn-view-all">
          {{ __('home.news.view_all') }}
        </a>
      </div>
    </div>
  </div>
</div>