<header id="header" class="header">
  <div class="header-content">
    <div class="container">
      <div class="row my-3">
        <div class="col-lg-7">
          <div class="text-container">
            <h1 class="wow animate__fadeInUp" data-wow-delay="1s" data-wow-duration="2s">
              <span class="turquoise">{{ __('home.hero.line_1') }}</span>
              <br>{{ __('home.hero.line_2') }}
            </h1>
            <h1 class="wow animate__fadeInUp" data-wow-delay="1s" data-wow-duration="2s">
              <span class="turquoise">{{ __('home.hero.line_3') }} </span>
              <br>{{ __('home.hero.line_4') }}
            </h1>
            <h1 class="wow animate__fadeInUp" data-wow-delay="1s" data-wow-duration="2s">
              <span class="turquoise">{{ __('home.hero.line_5') }}</span>
              <br>{{ __('home.hero.line_6') }}
            </h1>
            <a class="btn-solid-lg page-scroll wow animate__fadeInUp" data-wow-delay="1s" data-wow-duration="2s" href="{{ app_url('email-verification') }}" target="_blank">
              {{ __('home.hero.button_try_free') }}
            </a>
            <a class="btn-light-danger-lg page-scroll wow animate__fadeInUp" data-wow-delay="1s" data-wow-duration="2s" href="#watchDemo" target="_blank">
              {{ __('home.hero.button_watch') }}
            </a>
          </div>
          <!-- end of text-container -->
        </div>
        <!-- end of col -->
        <div class="col-lg-5">
          <div id="metaslider-id-452" style="width: 100%;" class="ml-slider-3-20-3 metaslider metaslider-flex metaslider-452 ml-slider">
            <div id="metaslider_container_452">
              <div id="metaslider_452">
                <ul aria-live="polite" class="slides">

                  <li style="display: block; width: 100%;" class="slide-659 ms-image">
                    <img width="394" height="797" src="{{ asset('images/hero-slider/'.app()->getLocale().'/step0.png') }}" class="slider-452 slide-660" alt="" rel="" title="step1" style="margin: 0 auto; width: 63.559777737946%" srcset="{{ asset('images/hero-slider/'.app()->getLocale().'/step0.png') }} 394w, {{ asset('images/hero-slider/'.app()->getLocale().'/step0.png') }} 148w" sizes="(max-width: 394px) 100vw, 394px" />
                  </li>

                  <li style="display: block; width: 100%;" class="slide-660 ms-image">
                    <img width="394" height="797" src="{{ asset('images/hero-slider/'.app()->getLocale().'/step1-1.png') }}" class="slider-452 slide-660" alt="" rel="" title="step1" style="margin: 0 auto; width: 63.559777737946%" srcset="{{ asset('images/hero-slider/'.app()->getLocale().'/step1-1.png') }} 394w, {{ asset('images/hero-slider/'.app()->getLocale().'/step1-1.png') }} 148w" sizes="(max-width: 394px) 100vw, 394px" />
                  </li>
                  <li style="display: none; width: 100%;" class="slide-661 ms-image">
                    <img width="394" height="730" src="{{ asset('images/hero-slider/'.app()->getLocale().'/step2-1.png') }}" class="slider-452 slide-660" alt="" rel="" title="step1" style="margin: 0 auto; width: 63.559777737946%" srcset="{{ asset('images/hero-slider/'.app()->getLocale().'/step2-1.png') }} 394w, {{ asset('images/hero-slider/'.app()->getLocale().'/step2-1.png') }} 148w" sizes="(max-width: 394px) 100vw, 394px" />
                  </li>
                  <li style="display: none; width: 100%;" class="slide-667 ms-image">
                    <img width="394" height="797" src="{{ asset('images/hero-slider/'.app()->getLocale().'/step4-2.png') }}" class="slider-452 slide-660" alt="" rel="" title="step1" style="margin: 0 auto; width: 63.559777737946%" srcset="{{ asset('images/hero-slider/'.app()->getLocale().'/step4-2.png') }} 394w, {{ asset('images/hero-slider/'.app()->getLocale().'/step4-2.png') }} 148w" sizes="(max-width: 394px) 100vw, 394px" />
                  </li>
                  <li style="display: none; width: 100%;" class="slide-668 ms-image">
                    <img width="394" height="797" src="{{ asset('images/hero-slider/'.app()->getLocale().'/step5-2.png') }}" class="slider-452 slide-660" alt="" rel="" title="step1" style="margin: 0 auto; width: 63.559777737946%" srcset="{{ asset('images/hero-slider/'.app()->getLocale().'/step5-2.png') }} 394w, {{ asset('images/hero-slider/'.app()->getLocale().'/step5-2.png') }} 148w" sizes="(max-width: 394px) 100vw, 394px" />
                  </li>
                  <li style="display: none; width: 100%;" class="slide-664 ms-image">
                    <img width="394" height="797" src="{{ asset('images/hero-slider/'.app()->getLocale().'/step6.png') }}" class="slider-452 slide-660" alt="" rel="" title="step1" style="margin: 0 auto; width: 63.559777737946%" srcset="{{ asset('images/hero-slider/'.app()->getLocale().'/step6.png') }} 394w, {{ asset('images/hero-slider/'.app()->getLocale().'/step6.png') }} 148w" sizes="(max-width: 394px) 100vw, 394px" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- end of col -->
      </div>
      <!-- end of row -->
    </div>
    <!-- end of container -->
  </div>
  <!-- end of header-content -->
</header>