<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="footer-col">
          <img class="img-fluid" src="{{ asset('images/Acomo-Logo-Hi-res-1024x232.png') }}" alt="" />
        </div>
      </div>
      <!-- end of col -->
      <div class="col-md-3">
        <div class="footer-col middle">
          <h5>{{ __('footer.column_1.title') }}</h5>
          <ul class="list-unstyled li-space-lg">
            <li class="media">
              <div class="media-body">
                <a href="#services">{{ __('footer.column_1.line_1') }}</a>
              </div>
            </li>
            <li class="media">
              <div class="media-body">
                <a href="#functions">{{ __('footer.column_1.line_2') }}</a>
              </div>
            </li>
            <li class="media">
              <div class="media-body">
                <a href="#news">{{ __('footer.column_1.line_3') }}</a>
              </div>
            </li>
            <li class="media">
              <div class="media-body">
                <a href="#faq">{{ __('footer.column_1.line_4') }}</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <!-- end of col -->
      <div class="col-md-3">
        <div class="footer-col last">
          <h5>{{ __('footer.column_2.title') }}</h5>
          <ul class="list-unstyled li-space-lg">
            <li class=address>
              {{ __('footer.column_2.line_1') }}
              {{-- </i>3-27-1, Shimorenjaku, Miataka, <br> Tokyo, Japan 181-0013 </li> --}}
            <li class=address>
              <a href="tel:003024630820">{{ __('footer.column_2.line_2') }}</a>
            </li>
            <li class=address>
              </i>
              <a href="mailto:office@checkinherenow.com">{{ __('footer.column_2.line_3') }}</a>
            </li>
            <li class=address>
              </i>{{ __('footer.column_2.line_4') }}</li>
          </ul>
        </div>
      </div>
      <!-- end of col -->
      <div class="col-md-3">
        <div class="mapouter">
          <div class="gmap_canvas">
            <iframe width="100%" height="200" id="gmap_canvas" src="https://maps.google.com/maps?q=3-27-1%2C%20Shimorenjaku%2C%20Miataka%2C%20Tokyo%2C%20Japan%20181-0013&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
          </div>
          <style>
            .mapouter {
              position: relative;
              text-align: right;
              height: 200px;
              width: 100%;
            }

            .gmap_canvas {
              overflow: hidden;
              background: none !important;
              height: 200px;
              width: 100%;
            }
          </style>
        </div>
      </div>
    </div>
    <!-- end of row -->
    <div class="row copyright pb-5 pt-5">
      <div class="col-12 text-center">
        <span class="pr-2 pl-2">© 2021 Acomo</span>
        {{-- |
        <a class="pr-2 pl-2" href="{{ url($locale.'/privacy-policy') }}">Privacy Policy</a> --}}
      </div>
    </div>
  </div>
  <!-- end of container -->
</div>
<!-- end of footer -->