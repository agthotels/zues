<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width" />
<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>Touchless Check-In Service</title>

<meta
  name="description"
  content='Introducing Touchless Check-in Service, can be used at Hotels and Tourist Spots. If you have a QR code and a smartphone, you can use it immediately after placing QR Code at the entrance or the reception. Monthly cost is low and customization is possible.'
/>
<meta name="keywords" content="Check-in, check-out, touchless, smartphone, kiosk, payment, QR code, PMS, integration" />
<meta property="og:image" content="{{ asset('images/acomo_logo_1000_1000.png') }}" />
<meta property="og:title" content='Touchless Check-In Service' />
<meta property="og:description" content='Introducing Touchless Check-in Service, can be used at Hotels and Tourist Spots. If you have a QR code and a smartphone, you can use it immediately after placing QR Code at the entrance or the reception. Monthly cost is low and customization is possible.' />
<meta name="msapplication-TileImage" content="{{ asset('images/cropped-thumbnail-1-270x270.png') }}" />

<link rel='stylesheet' href='{{ asset('css/bootstrap.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ asset('css/animate.min.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ asset('css/font-awesome/css/all.min.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ asset('css/swiper.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ asset('css/magnific-popup.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ asset('css/styles.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ asset('css/app.css') }}' type='text/css' media='all' />

<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>

<link rel="icon" href="{{ asset('images/cropped-thumbnail-1-32x32.png') }}" sizes="32x32" />
<link rel="icon" href="{{ asset('images/cropped-thumbnail-1-192x192.png') }}" sizes="192x192" />
<link rel="apple-touch-icon" href="{{ asset('images/cropped-thumbnail-1-180x180.png') }}" />

<script type='text/javascript' src='{{ asset('js/jquery.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('js/recaptcha-custom.js') }}'></script>

<!-- GA Google Analytics @ https://m0n.co/ga -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-165058219-2', 'auto');
  ga('send', 'pageview');
</script>

<!-- /GA Google Analytics @ https://m0n.co/ga -->
{!! htmlScriptTagJsApi() !!}
