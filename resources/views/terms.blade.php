<!DOCTYPE html>
<html lang="en-US">
<head>
  <x-header-root />
</head>

<body
  class="page-template page-template-page-home page-template-page-home-php page page-id-13 page-parent"
  data-spy="scroll" data-target=".fixed-top"
>

<!-- Top Nav -->
<x-nav />
<!-- /Top Nav -->

<!-- Content -->
<div class="container py-5 agreement">
  <div class="row pt-5">
    <div class="col-12 mx-auto">
      <h2 class="text-center pt-4">{{ __('terms.title') }}</h2>
      <p>{{ __('terms.desc_1') }}</p>
      <ol>
        <li>
            <h4>{{ __('terms.applications.title') }}</h4>
            <ol>
                <li>{{ __('terms.applications.l1') }}</li>
                <li>{{ __('terms.applications.l2') }}</li>
                <li>{{ __('terms.applications.l3') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.definitions.title') }}</h4>
            <p>{{ __('terms.definitions.desc') }}</p>
            <ol>
                <li>{{ __('terms.definitions.l1') }}</li>
                <li>{{ __('terms.definitions.l2') }}</li>
                <li>{{ __('terms.definitions.l3') }}</li>
                <li>{{ __('terms.definitions.l4') }}</li>
                <li>{{ __('terms.definitions.l5') }}</li>
                <li>{{ __('terms.definitions.l6') }}</li>
                <li>{{ __('terms.definitions.l7') }}</li>
                <li>{{ __('terms.definitions.l8') }}</li>
                <li>{{ __('terms.definitions.l9') }}</li>
                <li>{{ __('terms.definitions.l10') }}</li>
                <li>{{ __('terms.definitions.l11') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.registration.title') }}</h4>
            <ol>
                <li>{{ __('terms.registration.l1') }}</li>
                <li>{{ __('terms.registration.l2') }}</li>
                <li>{{ __('terms.registration.l3') }}</li>
                <li>
                    {{ __('terms.registration.l4.text') }}
                    <ol>
                        <li>{{ __('terms.registration.l4.a') }}</li>
                        <li>{{ __('terms.registration.l4.b') }}</li>
                        <li>{{ __('terms.registration.l4.c') }}</li>
                        <li>{{ __('terms.registration.l4.d') }}</li>
                        <li>{{ __('terms.registration.l4.e') }}</li>
                    </ol>
                </li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.responsibility.title') }}</h4>
            <ol>
                <li>{{ __('terms.responsibility.l1') }}</li>
                <li>{{ __('terms.responsibility.l2') }}</li>
                <li>{{ __('terms.responsibility.l3') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.password.title') }}</h4>
            <ol>
                <li>{{ __('terms.password.l1') }}</li>
                <li>{{ __('terms.password.l2') }}</li>
                <li>{{ __('terms.password.l3') }}</li>
                <li>{{ __('terms.password.l4') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.content.title') }}</h4>
            <ol>
                <li>
                    {{ __('terms.content.l1.text') }}
                    <ol>
                        <li>{{ __('terms.content.l1.a') }}</li>
                        <li>{{ __('terms.content.l1.b') }}</li>
                        <li>{{ __('terms.content.l1.c') }}</li>
                        <li>{{ __('terms.content.l1.d') }}</li>
                    </ol>
                </li>
                <li>{{ __('terms.content.l2') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.implementation.title') }}</h4>
            <ol>
                <li>{{ __('terms.implementation.l1') }}</li>
                <li>{{ __('terms.implementation.l2') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.fee.title') }}</h4>
            <ol>
                <li>{{ __('terms.fee.l1') }}</li>
                <li>{{ __('terms.fee.l2') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.payment.title') }}</h4>
            <ol>
                <li>{{ __('terms.payment.l1') }}</li>
                <li>{{ __('terms.payment.l2') }}</li>
                <li>{{ __('terms.payment.l3') }}</li>
                <li>{{ __('terms.payment.l4') }}</li>
                <li>{{ __('terms.payment.l5') }}</li>
                <li>{{ __('terms.payment.l6') }}</li>
                <li>{{ __('terms.payment.l7') }}</li>
                <li>{{ __('terms.payment.l8') }}</li>
                <li>{{ __('terms.payment.l9') }}</li>
                <li>{{ __('terms.payment.l10') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.payment-facility.title') }}</h4>
            <ol>
                <li>{{ __('terms.payment-facility.l1') }}</li>
                <li>{{ __('terms.payment-facility.l2') }}</li>
                <li>{{ __('terms.payment-facility.l3') }}</li>
                <li>{{ __('terms.payment-facility.l4') }}</li>
                <li>{{ __('terms.payment-facility.l5') }}</li>
                <li>{{ __('terms.payment-facility.l6') }}</li>
                <li>{{ __('terms.payment-facility.l7') }}</li>
                <li>
                    {{ __('terms.payment-facility.l8.text') }}
                    <ol>
                        <li>{{ __('terms.payment-facility.l8.a') }}</li>
                        <li>{{ __('terms.payment-facility.l8.b') }}</li>
                    </ol>
                </li>
                <li>{{ __('terms.payment-facility.l9') }}</li>
                <li>{{ __('terms.payment-facility.l10') }}</li>
                <li>{{ __('terms.payment-facility.l11') }}</li>
                <li>
                    {{ __('terms.payment-facility.l12.text') }}
                    <ol>
                        <li>{{ __('terms.payment-facility.l12.a') }}</li>
                        <li>{{ __('terms.payment-facility.l12.b') }}</li>
                        <li>{{ __('terms.payment-facility.l12.c') }}</li>
                    </ol>
                </li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.representations.title') }}</h4>
            <ol>
                <li>
                    {{ __('terms.representations.l1.text') }}
                    <ol>
                        <li>{{ __('terms.representations.l1.a') }}</li>
                        <li>{{ __('terms.representations.l1.b') }}</li>
                        <li>{{ __('terms.representations.l1.c') }}</li>
                        <li>{{ __('terms.representations.l1.d') }}</li>
                        <li>{{ __('terms.representations.l1.e') }}</li>
                    </ol>
                </li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.tool.title') }}</h4>
            <ol>
                <li>{{ __('terms.tool.l1') }}</li>
                <li>{{ __('terms.tool.l2') }}</li>
                <li>{{ __('terms.tool.l3') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.users.title') }}</h4>
            <ol>
                <li>{{ __('terms.users.l1') }}</li>
                <li>{{ __('terms.users.l2') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.report.title') }}</h4>
            <ol>
                <li>{{ __('terms.report.l1') }}</li>
                <li>{{ __('terms.report.l2') }}</li>
                <li>{{ __('terms.report.l3') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.service.title') }}</h4>
            <p>{{ __('terms.service.desc') }}</p>
        </li>
        <li>
            <h4>{{ __('terms.disclaimer.title') }}</h4>
            <ol>
                <li>{{ __('terms.disclaimer.l1') }}</li>
                <li>{{ __('terms.disclaimer.l2') }}</li>
                <li>{{ __('terms.disclaimer.l3') }}</li>
                <li>{{ __('terms.disclaimer.l4') }}</li>
                <li>{{ __('terms.disclaimer.l5') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.rights.title') }}</h4>
            <p>{{ __('terms.rights.desc') }}</p>
        </li>
        <li>
            <h4>{{ __('terms.confidentiality.title') }}</h4>
            <p>{{ __('terms.confidentiality.desc') }}</p>
        </li>
        <li>
            <h4>{{ __('terms.security.title') }}</h4>
            <ol>
                <li>{{ __('terms.security.l1') }}</li>
                <li>{{ __('terms.security.l2') }}</li>
                <li>{{ __('terms.security.l3') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.contract-period.title') }}</h4>
            <ol>
                <li>{{ __('terms.contract-period.l1') }}</li>
                <li>{{ __('terms.contract-period.l2') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.termination.title') }}</h4>
            <ol>
                <li>
                    {{ __('terms.termination.l1.text') }}
                    <ol>
                        <li>{{ __('terms.termination.l1.a') }}</li>
                        <li>{{ __('terms.termination.l1.b') }}</li>
                        <li>{{ __('terms.termination.l1.c') }}</li>
                        <li>{{ __('terms.termination.l1.d') }}</li>
                    </ol>
                </li>
                <li>{{ __('terms.termination.l2') }}</li>
                <li>{{ __('terms.termination.l3') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.suspension.title') }}</h4>
            <ol>
                <li>
                    {{ __('terms.suspension.l1.text') }}
                    <ol>
                        <li>{{ __('terms.suspension.l1.a') }}</li>
                        <li>{{ __('terms.suspension.l1.b') }}</li>
                        <li>{{ __('terms.suspension.l1.c') }}</li>
                        <li>{{ __('terms.suspension.l1.d') }}</li>
                    </ol>
                </li>
                <li>{{ __('terms.suspension.l2') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.anti-social.title') }}</h4>
            <ol>
                <li>{{ __('terms.anti-social.l1') }}</li>
                <li>{{ __('terms.anti-social.l2') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.end-service.title') }}</h4>
            <p>{{ __('terms.end-service.desc') }}</p>
        </li>
        <li>
            <h4>{{ __('terms.notification.title') }}</h4>
            <ol>
                <li>{{ __('terms.notification.l1') }}</li>
                <li>{{ __('terms.notification.l2') }}</li>
                <li>{{ __('terms.notification.l3') }}</li>
            </ol>
        </li>
        <li>
            <h4>{{ __('terms.change-agreement.title') }}</h4>
            <p>{{ __('terms.change-agreement.desc') }}</p>
        </li>
        <li>
            <h4>{{ __('terms.applied-laws.title') }}</h4>
            <p>{{ __('terms.applied-laws.desc') }}</p>
        </li>
      </ol>
      <p>
           <br>{{ __('terms.date_1') }}<br>
           {{ __('terms.date_2') }}<br>
           {{ __('terms.date_3') }}
      </p>
    </div>
  </div>
</div>
<!-- /Content -->

<!-- reCAPTCHA -->
<x-captcha-script />
<!-- /reCAPTCHA -->

<!-- Footer -->
<x-footer />
<!-- /Footer -->

<x-footer-script />

</body>
</html>
