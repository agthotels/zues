<!DOCTYPE html>
<html lang="en-US">

<head>
  <x-header-root />
</head>

<body class="page-template page-template-page-home page-template-page-home-php page page-id-13 page-parent" data-spy="scroll" data-target=".fixed-top">

  <!-- Top Nav -->
  <x-nav />
  <!-- /Top Nav -->

  <!-- Content -->
  <div class="container py-5 agreement">
    <div class="row pt-5">
      <div class="col-lg-6 mt-5 mx-auto">
        <h2>{{ __('watch-demo.title') }}</h2>
        @if(isset($email_sent) && $email_sent == 1)
        <div>
          <p class="h4 mb-3">{{ __('watch-demo.form.message_success_title') }}</p>
          <p>{{ __('watch-demo.form.message_success') }}</p>
        </div>
        <div class="text-center mt-5">
          <a href="/{{app()->getLocale()}}" class="btn btn-danger">{{ __('ask-demo.form.back') }}</a>
        </div>
        @else
        <div class="wpforms-container wpforms-container-full" id="wpforms">
          <form id="inquiry_form" class="wpforms-validate wpforms-form wpforms-ajax-form" method="post" enctype="multipart/form-data" action="{{ url($locale.'/watch-demo') }}">
            @csrf
            <input type="hidden" name="submit" value="1" />
            <div class="wpforms-head-container">
              <div class="wpforms-description">
                {{ __('watch-demo.description') }}
              </div>
            </div>
            <div class="wpforms-field-container">
              <div class="wpforms-field">
                <label class="wpforms-field-label">
                  {{ __('watch-demo.form.label_name') }} <span class="wpforms-required-label">*</span>
                </label>
                <input type="text" name="name" value="{{ old('name') }}" class="@error('name') wpforms-error @enderror" required />
                @error('name')
                <label class="wpforms-error">{{ $message }}</label>
                @enderror
              </div>
              <div class="wpforms-field">
                <label class="wpforms-field-label">
                  {{ __('watch-demo.form.label_company') }} <span class="wpforms-required-label">*</span>
                </label>
                <input type="text" name="company" value="{{ old('company') }}" required />
              </div>
              <div class="wpforms-field">
                <label class="wpforms-field-label">
                  {{ __('watch-demo.form.label_phone') }} <span class="wpforms-required-label">*</span>
                </label>
                <input type="number" pattern="\d*" name="phone" value="{{ old('phone') }}" class="@error('phone') wpforms-error @enderror" required />
                @error('phone')
                <label class="wpforms-error">{{ $message }}</label>
                @enderror
              </div>
              <div class="wpforms-field">
                <label class="wpforms-field-label">
                  {{ __('watch-demo.form.label_email') }} <span class="wpforms-required-label">*</span>
                </label>
                <input type="email" name="email" value="{{ old('email') }}" class="@error('email') wpforms-error @enderror" required />
                @error('email')
                <label class="wpforms-error">{{ $message }}</label>
                @enderror
              </div>
            </div>
            <div class="text-center mb-2">
              <input id="g_recaptcha_field" type="hidden" name="g-recaptcha-response" value="" />
              {!! htmlFormSnippet() !!}
            </div>
            @error('g-recaptcha-response')
              <label class="wpforms-error text-center">{{ $message }}</label>
            @enderror
            <div class="wpforms-submit-container">
              <button type="submit" class="wpforms-submit custom-btn-form" data-alt-text="{{ __('watch-demo.form.status_sending') }}">
                {{ __('watch-demo.form.button_submit') }}
                <img src="{{ asset('images/submit-spin.svg') }}" class="wpforms-submit-spinner" style="display: none;" width="26" height="26" />
              </button>
            </div>
          </form>
        </div>
        @endif
      </div>
    </div>
  </div>

  @if(!isset($email_sent))
  <!-- Footer -->
  <x-footer />
  <!-- /Footer -->
  @endif

  <x-footer-script />

</body>

</html>