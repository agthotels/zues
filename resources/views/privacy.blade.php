<!DOCTYPE html>
<html lang="en-US">
<head>
  <x-header-root />
</head>

<body
  class="page-template page-template-page-home page-template-page-home-php page page-id-13 page-parent"
  data-spy="scroll" data-target=".fixed-top"
>

<!-- Top Nav -->
<x-nav />
<!-- /Top Nav -->

<!-- Content -->
<div class="container py-5 agreement">
  <div class="row pt-5">
    <div class="col-12 mx-auto">
      <h2 class="text-center pt-4">{{ __('privacy.title') }}</h2>
      <p>{{ __('privacy.desc') }}</p>
      <ol>
        <li>
          <h4>{{ __('privacy.objectives.title') }}</h4>
          <p>{{ __('privacy.objectives.desc') }}</p>
          <ol>
            <li>{{ __('privacy.objectives.l1') }}</li>
            <li>{{ __('privacy.objectives.l2') }}</li>
            <li>{{ __('privacy.objectives.l3') }}</li>
            <li>{{ __('privacy.objectives.l4') }}</li>
            <li>{{ __('privacy.objectives.l5') }}</li>
            <li>{{ __('privacy.objectives.l6') }}</li>
            <li>{{ __('privacy.objectives.l7') }}</li>
            <li>{{ __('privacy.objectives.l8') }}</li>
            <li>{{ __('privacy.objectives.l9') }}</li>
            <li>{{ __('privacy.objectives.l0') }}</li>
            <li>{{ __('privacy.objectives.l11') }}</li>
            <li>{{ __('privacy.objectives.l12') }}</li>
            <li>{{ __('privacy.objectives.l13') }}</li>
          </ol>
        </li>
        <li>
          <h4>{{ __('privacy.acquisition.title') }}</h4>
          <p>{{ __('privacy.acquisition.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.provision.title') }}</h4>
          <p>{{ __('privacy.provision.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.management.title') }}</h4>
          <p>{{ __('privacy.management.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.personal-info.title') }}</h4>
          <p>{{ __('privacy.personal-info.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.provision-personal-info.title') }}</h4>
          <p>{{ __('privacy.provision-personal-info.desc') }}</p>
          <ol>
            <li>{{ __('privacy.provision-personal-info.l1') }}</li>
            <li>{{ __('privacy.provision-personal-info.l2') }}</li>
            <li>{{ __('privacy.provision-personal-info.l3') }}</li>
            <li>{{ __('privacy.provision-personal-info.l4') }}</li>
            <li>{{ __('privacy.provision-personal-info.l5') }}</li>
            <li>{{ __('privacy.provision-personal-info.l6') }}</li>
            <li>{{ __('privacy.provision-personal-info.l7') }}</li>
          </ol>
        </li>
        <li>
          <h4>{{ __('privacy.sensitive-info.title') }}</h4>
          <p>{{ __('privacy.sensitive-info.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.consignment-info.title') }}</h4>
          <p>{{ __('privacy.consignment-info.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.disclosure-info.title') }}</h4>
          <p>{{ __('privacy.disclosure-info.desc') }}</p>
          <ol>
            <li>{{ __('privacy.disclosure-info.l1') }}</li>
            <li>{{ __('privacy.disclosure-info.l2') }}</li>
            <li>{{ __('privacy.disclosure-info.l3') }}</li>
            <li>{{ __('privacy.disclosure-info.l4') }}</li>
            <li>
              <p>
                <div>{{ __('privacy.disclosure-info.l5.desc-1a') }}</div>
                <div><b>{{ __('privacy.disclosure-info.l5.desc-1b.text') }}</b></div>
                <div>{{ __('privacy.disclosure-info.l5.desc-1b.desc') }}</div>
              </p>
              <ul>
                <li>{{ __('privacy.disclosure-info.l5.desc-1b.a') }}</li>
                <li>{{ __('privacy.disclosure-info.l5.desc-1b.b') }}</li>
                <li>{{ __('privacy.disclosure-info.l5.desc-1b.c') }}</li>
                <li>{{ __('privacy.disclosure-info.l5.desc-1b.d') }}</li>
              </ul>
              <p>
                <div><b>{{ __('privacy.disclosure-info.l5.desc-2a') }}</b></div>
                <div>{{ __('privacy.disclosure-info.l5.desc-2b.text') }}</div>
              </p>
              <ul>
                <li>{{ __('privacy.disclosure-info.l5.desc-2b.a') }}</li>
                <li>{{ __('privacy.disclosure-info.l5.desc-2b.b') }}</li>
                <li>{{ __('privacy.disclosure-info.l5.desc-2b.c') }}</li>
              </ul>
            </li>
            <li>
              <p>
                <div>{{ __('privacy.disclosure-info.l6.title') }}</div>
                <div>{{ __('privacy.disclosure-info.l6.desc') }}</div>
              </p>
            </li>
            <li>
              <p>
                <div>{{ __('privacy.disclosure-info.l7.title') }}</div>
                <div>{{ __('privacy.disclosure-info.l7.desc') }}</div>
              </p>
              <ul>
                <li>{{ __('privacy.disclosure-info.l7.a') }}</li>
                <li>{{ __('privacy.disclosure-info.l7.b') }}</li>
                <li>{{ __('privacy.disclosure-info.l7.c') }}</li>
                <li>{{ __('privacy.disclosure-info.l7.d') }}</li>
                <li>{{ __('privacy.disclosure-info.l7.e') }}</li>
                <li>{{ __('privacy.disclosure-info.l7.f') }}</li>
                <li>{{ __('privacy.disclosure-info.l7.g') }}</li>
              </ul>
            </li>
          </ol>
        </li>
        <li>
          <h4>{{ __('privacy.cookie.title') }}</h4>
          <p>{{ __('privacy.cookie.desc') }}</p>
          <ol>
            <li>{{ __('privacy.cookie.l1') }}</li>
            <li>{{ __('privacy.cookie.l2') }}</li>
            <li>{{ __('privacy.cookie.l3') }}</li>
          </ol>
        </li>
        <li>
          <h4>{{ __('privacy.access-log.title') }}</h4>
          <p>{{ __('privacy.access-log.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.revision.title') }}</h4>
          <p>{{ __('privacy.revision.desc') }}</p>
        </li>
        <li>
          <h4>{{ __('privacy.inquiry.title') }}</h4>
          <p>
            {{ __('privacy.inquiry.desc1') }}<br />
            {{ __('privacy.inquiry.desc2') }}<br />
            {{ __('privacy.inquiry.desc3') }}<br />
            {{ __('privacy.inquiry.desc4') }}
          </p>
        </li>
      </ol>
    </div>
  </div>
</div>
<!-- /Content -->

<!-- reCAPTCHA -->
<x-captcha-script />
<!-- /reCAPTCHA -->

<!-- Footer -->
<x-footer />
<!-- /Footer -->

<x-footer-script />

</body>
</html>
