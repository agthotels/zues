# Install PHP
FROM php:8.2-cli-alpine

ARG USER_NAME=www-data
ARG USER_ID=33
ARG GROUP_ID=33

# Install php extensions
RUN apk update
RUN apk add \
    coreutils \
    zlib \
    libpng-dev \
    libwebp-dev \
    libjpeg-turbo-dev \
    freetype-dev
RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp
RUN docker-php-ext-install exif pdo_mysql gd

# Install tools for troubleshooting
RUN apk add iputils mysql-client

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Create process user
RUN addgroup -g ${GROUP_ID} ${USER_NAME}
RUN adduser -S -u ${USER_ID} ${USER_NAME} -G ${USER_NAME}

COPY ./zz-docker.conf /usr/local/etc/php-fpm.d/

# Change current user to www
USER root

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]